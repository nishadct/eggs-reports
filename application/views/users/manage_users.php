<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Users</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Create</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
           <div class="col-lg-4">
           		<form action="" method="post" class="form-horizontal" id="formUsers">
                <div class="card">
                    <div class="card-header">
                        <strong>Create</strong> Users
                    </div>
                    <div class="card-body card-block">
                        
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text_fname" class=" form-control-label">Fisrt Name<sup>*</sup></label></div>
                                <div class="col-12 col-md-9"><input type="text" id="text_fname" name="text_fname" placeholder="First Name" class="form-control"></div>
                            </div>
                             <div class="row form-group">
                                <div class="col col-md-3"><label for="text_lname" class=" form-control-label">Last Name</label></div>
                                <div class="col-12 col-md-9"><input type="text" id="text_lname" name="text_lname" placeholder="Last Name" class="form-control"></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="email_input" class=" form-control-label">Email<sup>*</sup></label></div>
                                <div class="col-12 col-md-9"><input type="email" id="email_input" name="email_input" placeholder="Enter Email" class="form-control"></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="password_input" class=" form-control-label">Password<sup>*</sup></label></div>
                                <div class="col-12 col-md-9"><input type="password" id="password_input" name="password_input" placeholder="Password" class="form-control"><small class="help-block form-text">Please enter a complex password</small></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="select_company" class=" form-control-label">Company<sup>*</sup></label></div>
                                <div class="col-12 col-md-9">
                                    <select name="select_company" id="select_company" class="form-control">
                                    	<option value="">Please select</option>
                                    	<?php foreach($companies as $key=>$value){?>
                                        <option value="<?php echo $value;?>"><?php echo $value;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="text_role" class=" form-control-label">User Role<sup>*</sup></label></div>
                                <div class="col-12 col-md-9">
                                    <select name="text_role" id="text_role" class="form-control">
                                    	<option value="">Please select</option>
                                        <option value="1">Admin</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                    </select>
                                </div>
                            </div>
                        
                    </div>
                    <div class="card-footer text-right" >
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Create
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Reset
                        </button>
                    </div>
                </div>
                </form>
            </div>
            
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Users</strong>
                    </div>
                    <div class="card-body">
                    	<div class="table-responsive">
                            <table id="brand_table" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>User Type</th>
                                        <th>Company</th>
                                        <th>User Status</th>
                                        <th>Created On</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<style>
.dataTables_filter,.dataTables_paginate {
   float: right !important;
}
</style>
<script>
jQuery(document).ready(function () {
		//validating and submit
		jQuery("#formUsers").validate({
			rules: {
				text_fname: "required",
				password_input: "required",
				select_company:"required",
				email_input: {
					required: true,
					email: true
				}
			},
			messages: {
				email_input: "Please enter a valid email address",
				password_input: "Please enter your password",
				text_fname: "Please enter first name",
				select_company: "Please select company"
			}
		});
		
		//on submission
		jQuery(document).on("submit", "#formUsers", function (event) {
			event.preventDefault();
			if(jQuery("#formUsers").valid() == true){
				//loading spinner
				run_waitMe('#formUsers','bounce');
				var text_fname = jQuery('#text_fname').val();
				var text_lname = jQuery('#text_lname').val();
				var email_input = jQuery('#email_input').val();
				var password_input = jQuery('#password_input').val();
				var select_company = jQuery('#select_company').val();
                var text_role = jQuery('#text_role').val();
				jQuery.ajax({
					url: BASE_URL+'Users/manage',
					type: 'post',
					dataType: 'json',
					//data: $(this).serialize(),
					data: { text_role:text_role, text_fname: text_fname, text_lname: text_lname,email_input: email_input, password_input: password_input, select_company: select_company} ,
					success: function(data) {
						//close spinner
						jQuery('#formUsers').waitMe('hide');
						if(data.status == 'success'){
							jQuery.notify({message: data.message},{type: "success"});
							window.location.href = BASE_URL+data.url;
						}else{
							jQuery.notify({message: data.message},{type: 'danger'});
						}
					}
				});
			}else{
				jQuery.notify({message: 'Fill all required fields.'},{type: 'danger'});
			}
		});
		
		//datatable
		var tableDt = jQuery("#brand_table").dataTable({
			oLanguage: {
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			ajax: {
				"url": BASE_URL+"/Users/view", 
				"type": "POST"
			},
			/*columns: [
				{"data": null},
				{"data": "evb_VendBrandId"},
				{"data": "evb_VendBrandName"}
			],*/
			order: [[0, 'asc']],
			iDisplayLength: -1,
			aLengthMenu: [
				[25, 50, 100, 200, -1],
				[25, 50, 100, 200, "All"]
			]
		});
	});
</script>