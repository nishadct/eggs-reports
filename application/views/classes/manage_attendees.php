<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Class Attendees</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="float-right">
            
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Attendees</strong>
                    </div>
                    <div class="card-body">
                    	<div class="table-responsive">
                            <table id="attendee_table" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Class Name</th>
                                        <th>Class Instructor</th>
                                        <th>Class Date</th>
                                        <th>Attendee Name</th>
                                        <th>Attendee Email</th>
                                        <th>On</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<style>
.dataTables_filter,.dataTables_paginate {
   float: right !important;
}
</style>
<script>
jQuery(document).ready(function () {
    
		//datatable
		var tableDt = jQuery("#attendee_table").dataTable({
			oLanguage: {
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			ajax: {
				"url": BASE_URL+"/manageattendees", 
				"type": "POST"
			},
            /*columns: [
                { data: "clink_id" },
                { data: "class_name" },
                { data: "clink_date" },
                { data: "class_instructor" },
                { data: "clink_status" },
                { data: "clink_created_on" },
                { data: "name" },
                { data: "Link" },
            ],*/
			order: [[0, 'asc']],
			iDisplayLength: -1,
			aLengthMenu: [
				[25, 50, 100, 200, -1],
				[25, 50, 100, 200, "All"]
			]
		});
	});
</script>