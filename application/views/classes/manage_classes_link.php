<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Generate Class Link</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="float-right">
            <a href="javascript:void(0)" class="btn btn-success mt-2" data-toggle="modal" data-target="#largeModal">Create Link</a>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Generated Links</strong>
                    </div>
                    <div class="card-body">
                    	<div class="table-responsive">
                            <table id="class_link_table" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Class Name</th>
                                        <th>Class Date</th>
                                        <th>Class Instructor</th>
                                        <th>Status</th>
                                        <th>Created By</th>
                                        <th>Created On</th>
                                        <th>Link</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Create Links</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" class="form-horizontal" id="formClassLinks">
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="class_id" class=" form-control-label">Select Class<sup>*</sup></label></div>
                        <div class="col-12 col-md-9">
                            <select name="class_id" id="class_id" class="form-control">
                                <option value="">Please select</option>
                                <?php foreach($classess as $class){?>
                                <option value="<?php echo $class['class_id'];?>"><?php echo $class['class_name'].' ( '.$class['class_instructor'].' ) ';?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="class_date" class=" form-control-label">Date<sup>*</sup></label></div>
                        <div class="col-12 col-md-9"><input type="text" id="class_date" name="class_date" placeholder="Date" class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="link_status" class=" form-control-label">Status<sup>*</sup></label></div>
                        <div class="col-12 col-md-9">
                            <select name="link_status" id="link_status" class="form-control">
                                <option value="">Please select</option>
                                <?php foreach($statuses as $key=>$value){?>
                                <option value="<?php echo $key;?>" <?php echo $key == 'a' ? 'selected': ''; ?>><?php echo $value;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Generate Link</button>
                </div>
            </form>
        </div>
    </div>
</div>

<style>
.dataTables_filter,.dataTables_paginate {
   float: right !important;
}
</style>
<script>
jQuery(document).ready(function () {
    /*jQuery('#class_date').datepicker({
        uiLibrary: 'bootstrap4'
    });*/
    jQuery('#class_date').datetimepicker({format: 'YYYY-MM-DD'});
		//validating and submit
		jQuery("#formClassLinks").validate({
			rules: {
				class_id: "required",
				class_date: "required",
				link_status:"required",
			}
		});
		
		//on submission
		jQuery(document).on("submit", "#formClassLinks", function (event) {
			event.preventDefault();
			if(jQuery("#formClassLinks").valid() == true){
				//loading spinner
				run_waitMe('#formClassLinks','bounce');
				var class_id = jQuery('#class_id').val();
				var class_date = jQuery('#class_date').val();
				var link_status = jQuery('#link_status').val();
				jQuery.ajax({
					url: BASE_URL+'manageclasslinks',
					type: 'post',
					dataType: 'json',
					data: { class_id:class_id, class_date: class_date, link_status: link_status} ,
					success: function(data) {
						//close spinner
						jQuery('#formClassLinks').waitMe('hide');
						if(data.status == 'success'){
							jQuery.notify({message: data.message},{type: "success"});
							window.location.href = BASE_URL+data.url;
						}else{
							jQuery.notify({message: data.message},{type: 'danger'});
						}
					}
				});
			}else{
				jQuery.notify({message: 'Fill all required fields.'},{type: 'danger'});
			}
		});
		
		//datatable
		var tableDt = jQuery("#class_link_table").dataTable({
			oLanguage: {
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			ajax: {
				"url": BASE_URL+"/ens/Ensclasses/linkstable", 
				"type": "POST"
			},
            columns: [
                { data: "clink_id" },
                { data: "class_name" },
                { data: "clink_date" },
                { data: "class_instructor" },
                { data: "clink_status" },
                { data: "clink_created_on" },
                { data: "name" },
                { data: "Link" },
            ],
			order: [[0, 'asc']],
			iDisplayLength: -1,
			aLengthMenu: [
				[25, 50, 100, 200, -1],
				[25, 50, 100, 200, "All"]
			]
		});
        //Copy to clipboard function
        function copyToClipboard(text) {
            var sampleTextarea = document.createElement("textarea");
            document.body.appendChild(sampleTextarea);
            sampleTextarea.value = text; //save main text in it
            sampleTextarea.select(); //select textarea contenrs
            document.execCommand("copy");
            document.body.removeChild(sampleTextarea);
        } 
        //copy to clipboard
        jQuery(document).on("click", ".copy_to_clipboard", function (event) {
			event.preventDefault();
            var id = jQuery(this).attr('data-id');
            if(id > 0){
                copyToClipboard( jQuery('#'+id).val() );
                jQuery.notify({message: 'Link Copied.'},{type: 'success'});
            }else{
                jQuery.notify({message: 'Some error occured!'},{type: 'danger'});
            }
		});
	});
</script>