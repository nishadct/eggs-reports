<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Classes</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">Create / Edit</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
           <div class="col-lg-4">
           		<form action="" method="post" class="form-horizontal" id="formClasses">
                <div class="card">
                    <div class="card-header">
                        <strong>Create</strong> Classes
                    </div>
                    <div class="card-body card-block">
                        
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="class_name" class=" form-control-label">Class Name<sup>*</sup></label></div>
                                <div class="col-12 col-md-9"><input type="text" id="class_name" name="class_name" placeholder="First Name" class="form-control"></div>
                            </div>
                             <div class="row form-group">
                                <div class="col col-md-3"><label for="class_instructor" class=" form-control-label">Instructor Name</label></div>
                                <div class="col-12 col-md-9"><input type="text" id="class_instructor" name="class_instructor" placeholder="Last Name" class="form-control"></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3"><label for="class_status" class=" form-control-label">Status<sup>*</sup></label></div>
                                <div class="col-12 col-md-9">
                                    <select name="class_status" id="class_status" class="form-control">
                                    	<option value="">Please select</option>
                                    	<?php foreach($statuses as $key=>$value){?>
                                        <option value="<?php echo $key;?>"><?php echo $value;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                    </div>
                    <div class="card-footer text-right" >
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Create
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Reset
                        </button>
                    </div>
                </div>
                </form>
            </div>
            
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Classes</strong>
                    </div>
                    <div class="card-body">
                    	<div class="table-responsive">
                            <table id="class_table" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Class Name</th>
                                        <th>Class Instructor</th>
                                        <th>Class Status</th>
                                        <th>Created On</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<style>
.dataTables_filter,.dataTables_paginate {
   float: right !important;
}
</style>
<script>
jQuery(document).ready(function () {
		//validating and submit
		jQuery("#formClasses").validate({
			rules: {
				class_name: "required",
				class_instructor: "required",
				class_status:"required",
			}
		});
		
		//on submission
		jQuery(document).on("submit", "#formClasses", function (event) {
			event.preventDefault();
			if(jQuery("#formClasses").valid() == true){
				//loading spinner
				run_waitMe('#formClasses','bounce');
				var class_name = jQuery('#class_name').val();
				var class_instructor = jQuery('#class_instructor').val();
				var class_status = jQuery('#class_status').val();
				jQuery.ajax({
					url: BASE_URL+'manageclasses',
					type: 'post',
					dataType: 'json',
					data: { class_name:class_name, class_instructor: class_instructor, class_status: class_status} ,
					success: function(data) {
						//close spinner
						jQuery('#formClasses').waitMe('hide');
						if(data.status == 'success'){
							jQuery.notify({message: data.message},{type: "success"});
							window.location.href = BASE_URL+data.url;
						}else{
							jQuery.notify({message: data.message},{type: 'danger'});
						}
					}
				});
			}else{
				jQuery.notify({message: 'Fill all required fields.'},{type: 'danger'});
			}
		});
		
		//datatable
		var tableDt = jQuery("#class_table").dataTable({
			oLanguage: {
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			ajax: {
				"url": BASE_URL+"/ens/Ensclasses/view", 
				"type": "POST"
			},
			/*columns: [
				{"data": null},
				{"data": "evb_VendBrandId"},
				{"data": "evb_VendBrandName"}
			],*/
			order: [[0, 'asc']],
			iDisplayLength: -1,
			aLengthMenu: [
				[25, 50, 100, 200, -1],
				[25, 50, 100, 200, "All"]
			]
		});
	});
</script>