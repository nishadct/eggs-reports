<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Various Products Reports</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
						<div class="row" id="report_row">
							<div class="col-12">
								<div class="alert alert-success d-none" id="msginfo"></div>
							</div>
							
							<div class="col-12 col-md-4 text-center">
								<a href="javascript:void(0)" data-type="full" class="btn btn-ssm btn-success exportreports">Export Full Products</a>
							</div>
							<div class="col-12 col-md-4 text-center">
								<a href="javascript:void(0)" data-type="oos" class="btn btn-ssm btn-success exportreports">OOS Products</a>
							</div>
                            <div class="col-12 col-md-4 text-center">
								<a href="javascript:void(0)" data-type="img" class="btn btn-ssm btn-success exportreports">Image Missing Products</a>
							</div>
						</div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->

<script type="text/javascript">
    jQuery(document).ready(function () {
		
		//filter
		jQuery(document).on("click",".exportreports",function() {
            var report_type = jQuery(this).attr("data-type");
			run_waitMe('#report_row','bounce');
			var messgaeinfo = 'Query is executing, this may take some time. FIle will automatically download once done.';
			jQuery.notify({message: messgaeinfo},{type: "success"});
			window.location.href = BASE_URL+'reports/exports/'+report_type;
			setTimeout(function() {
                 jQuery('#report_row').waitMe('hide');
				 jQuery('#msginfo').removeClass('d-none').html('Please wait untill auto download starts..');
				 
            }, 10000);
		});
    });
</script>