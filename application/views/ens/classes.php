<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Classes & Workshops</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">View</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
						<input type="hidden" id="datetimeforfilename" value="<?php echo date('Y-m-d-His');?>"/>
						<div id="printbar" class="my-3" style="float:right;"></div>
                   	 	<!--<div class="row">
                            <div class="col-md-3">
                            	<div class="form-group">
                                	<label for="product_website_status" class=" form-control-label">Website Status</label>
                                    <select id="product_website_status" class="form-control">
                                        <option value="">--All--</option>
                                        <option value="1">Synced</option>
                                        <option value="0">Not Synced</option>
                                    </select>
                                </div>
                            	
                            </div>
                            <div class="col-md-3">
                            	<div class="form-group">
                                	<label for="product_stock_status" class=" form-control-label">Stock Status</label>
                                    <select id="product_stock_status" class="form-control">
                                        <option value="">--All--</option>
                                        <option value="instock">instock</option>
                                        <option value="outstock">Out of stock</option>
                                    </select>
                                </div>
                            </div>
                           
                            <div class="col-md-3" style="padding-top:30px;">
                            	<button type="button" class="btn btn-info" id="filter">Filter</button>
                            </div>
                        </div>
                    	<hr />-->
						<?php /* ?>
						<div class="row">
							<div class="col-md-6 col-12">
								<div class="form-group">
									<label for="class_title" class=" form-control-label">Select Class/Workshop Name</label>
									<select id="class_title" class="form-control">
										<option value="">--All--</option>
										<?php foreach($classes as $key=>$classname){
											echo "<option value='$classname'>$classname</option>";
										}?>
									</select>
								</div>
							</div>
							<div class="col-md-3" style="padding-top:30px;">
                            	<button type="button" class="btn btn-info" id="filter">Filter</button>
                            </div>
						</div>
						<?php */ ?>
                    	<div class="table-responsive">
                            <table id="big_table" class="table table-bordered">
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!-- .animated -->
</div><!-- .content -->
<style>
.dataTables_filter,.dataTables_paginate {
   float: right !important;
}
.dataTables_filter{
	margin-top: -35px;
}
/*div.dt-buttons {
    float: none !important;
	display:block;
	text-align: center !important;
}*/
#big_table_info{
	color: green !important;
    font-weight: 600 !important;
}
</style>
<script type="text/javascript">
    jQuery(document).ready(function () {
		var datetimeforfilename = jQuery('#datetimeforfilename').val();
		var tableDt = jQuery("#big_table").DataTable({
			oLanguage: {
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			searching: false,
			ajax: {
				"url": BASE_URL+"classesworkshops", 
				"type": "POST",
				"data": function ( d ) {
					d.class_title = '';
					//d.class_title = jQuery('#class_title').val();
					//d.product_stock_status = jQuery('#product_stock_status').val();
				}
			},
			columns: [
				{ data: 'name', title: 'User Name',"orderable": false,"searchable": false},
				{ data: 'email', title: 'User Email',"orderable": false,"searchable": false},
				{ data: 'telephone', title: 'Telephone',"orderable": false,"searchable": false},
				{ data: 'course-name', title: 'Class Name',"orderable": false,"searchable": false},
				{ data: 'course-date', title: 'Class Date',"orderable": false,"searchable": false},
				{ data: 'course-people-count', title: 'No.of People Attending',"orderable": false,"searchable": false},
				//{ data: 'course-url', title: 'Course URL'},
				{ 	"data": "course_url",title: 'Class URL',"orderable": false,"searchable": false,
					"render": function(data,type,row,meta) { 
						var a = '<a target="_blank" href="'+row.course_url+'" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>'; 
						return a;
					}
				},
				{ data: 'form_date', title: 'Registered On'}	
			],
			order: [['7', 'desc']],
			iDisplayLength: 25,
			aLengthMenu: [
				[25, 50, 100, 200, -1],
				[25, 50, 100, 200, "All"]
			],
			dom: 'lBfrtip',
			buttons: [
			   {
					extend: 'copy',
					footer: false,
					exportOptions: {
						columns: [0,1,2,3,4,5,7]
					},
					title: 'Classes-Wokshops-'+datetimeforfilename
			   },{
				   	extend: 'csv',
					footer: false,
					exportOptions: {
						columns: [0,1,2,3,4,5,7]
					},
					title: 'Classes-Wokshops-'+datetimeforfilename
			   },{
				   	extend: 'excel',
					footer: false,
					exportOptions: {
						columns: [0,1,2,3,4,5,7]
					},
					title: 'Classes-Wokshops-'+datetimeforfilename
			   }        
			]
		});
		tableDt.buttons().container().appendTo(jQuery('#printbar'));
		//filter
		jQuery(document).on("click","#filter",function() {
			tableDt.draw();
		});
    });
</script>