<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<?php 
$this->load->view('layouts/partials/header');
if(!empty($css)){
    foreach($css as $key=>$value){
        echo '<link rel="stylesheet" href="'.$value.'">';
    }
}
?>  
<body>
<?php $this->load->view('layouts/partials/left_panel');?>  
<div id="right-panel" class="right-panel"> 
	<?php $this->load->view('layouts/partials/body_header');?>   
    <!-- PAGE CONTENT BEGINS -->
	<?php echo $contents;?>
    <!-- PAGE CONTENT ENDS -->
    
    
</div>
<?php 
$this->load->view('layouts/partials/footer_scripts');
if(!empty($js)){
    foreach($js as $key=>$value){
        echo '<script src="'.$value.'"></script>';
    }
}
?>  
</body>

</html>