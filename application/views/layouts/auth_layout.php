<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<?php $this->load->view('layouts/partials/header');?>  
<body class="bg-dark">
    <!-- PAGE CONTENT BEGINS -->
	<?php echo $contents;?>
    <!-- PAGE CONTENT ENDS -->
<?php $this->load->view('layouts/partials/footer_scripts');?>  
</body>

</html>