<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Eggs &amp; Soldiers - Natural &amp; Eco-friendly Products for Mum, Baby &amp; Toddler in Dubai, Abu Dhabi and all UAE</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Roboto+Slab%3A200%2C300%2C400%2C500%2C600%2C700%2C800%7CRaleway%3A400%2C700%7COpen+Sans%3A400%2C600%2C700&#038;ver=5.8.3' media='all' />
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/');?>css/waitMe.css">
   
    
    <!--<script src="<?php //echo base_url('assets/');?>js/jquery-3.4.1.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="<?php echo base_url('assets/');?>js/waitMe.js"></script>
	<script src="<?php echo base_url('assets/');?>js/bootstrap-notify.js"></script>
    <script src="<?php echo base_url('assets/');?>js/jquery.validate.js"></script>
    <script>
		var BASE_URL = '<?php echo base_url();?>';
		//wait me ::loading
		function run_waitMe(selector,effect){
			jQuery(selector).waitMe({
				effect:effect
			});
		}
	</script>
</head>