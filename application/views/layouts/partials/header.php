<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">

    <link rel="stylesheet" href="<?php echo base_url('assets/');?>vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>vendors/jqvmap/dist/jqvmap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.0/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">

    <link rel="stylesheet" href="<?php echo base_url('assets/');?>css/style.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/');?>css/waitMe.css">
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/');?>css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>vendors/chosen/chosen.min.css">
    
    <script src="<?php echo base_url('assets/');?>js/jquery-3.4.1.min.js"></script>
    <script src="<?php echo base_url('assets/');?>js/waitMe.js"></script>
	<script src="<?php echo base_url('assets/');?>js/bootstrap-notify.js"></script>
    <script src="<?php echo base_url('assets/');?>js/jquery.validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/');?>js/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo base_url('assets/');?>vendors/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url('assets/');?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/');?>vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url('assets/');?>vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('assets/');?>vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>

    <script>
		var BASE_URL = '<?php echo base_url();?>';
		//wait me ::loading
		function run_waitMe(selector,effect){
			jQuery(selector).waitMe({
				effect:effect
			});
		}
		jQuery(document).ready(function() {
			jQuery(".standardSelect").chosen({
				disable_search_threshold: 10,
				no_results_text: "Oops, nothing found!",
				width: "100%"
			});
		});
	</script>

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

</head>