<!-- Left Panel -->
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="./"><img src="<?php echo base_url('assets/');?>images/logo-me.png" alt="Logo"></a>
            <a class="navbar-brand hidden" href="./"><img src="<?php echo base_url('assets/');?>images/logo-me2.png" alt="Logo"></a>
        </div>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <?php $access_users = [1,2]; ?>
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="<?php echo base_url('Dashboard');?>"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>
                <h3 class="menu-title">EnS Reports</h3><!-- /.menu-title -->
                <li>
                    <a href="<?php echo base_url('classesworkshops');?>"> <i class="menu-icon fa fa-briefcase"></i>Classes & Workshops </a>
                </li>
                <?php 
                if ( in_array($this->session->sessUserType, $access_users) ){
                ?>
                <li>
                    <a href="<?php echo base_url('waitlist');?>"> <i class="menu-icon fa fa-users"></i>Product Waitlist </a>
                </li>
				<li>
                    <a href="<?php echo base_url('external-classes');?>"> <i class="menu-icon fa fa-briefcase"></i>External Classes & Workshops </a>
                </li>
				<li>
                    <a href="<?php echo base_url('classes-interests');?>"> <i class="menu-icon fa fa-briefcase"></i>Classes & Workshop Interests </a>
                </li>
                <li>
                    <a href="<?php echo base_url('reports');?>"> <i class="menu-icon fa fa-arrow-right"></i>Product Reports</a>
                </li>
                <?php
                } ?>
                <h3 class="menu-title">EnS Classes</h3>
                <li>
                    <a href="<?php echo base_url('manageclasses');?>"> <i class="menu-icon fa fa-briefcase"></i>Classes</a>
                </li>
                <li>
                    <a href="<?php echo base_url('manageclasslinks');?>"> <i class="menu-icon fa fa-link"></i>Manage Links</a>
                </li>
                <li>
                    <a href="<?php echo base_url('manageattendees');?>"> <i class="menu-icon fa fa-table"></i>Manage Attendees</a>
                </li>
                <?php
				if($this->session->sessUserType == 1) {?>
                <h3 class="menu-title">Admin</h3><!-- /.menu-title -->
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users"></i>Users</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-user"></i><a href="<?php echo base_url('Users/manage');?>">Manage</a></li>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside>