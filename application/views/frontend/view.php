<div class="row">
    <div class="offset-lg-2 col-lg-8 col-sm-12 col-12 border rounded main-section">
        <div class="row">
            <div class="col align-self-center">
                <h5 class="">Participant Disclaimer </h5>
                <p class="info">Class: <span><?php echo $linkdetails['class_name'];?></span></p>
                <p class="info">Date: <span><?php echo date("d/m/Y", strtotime(date('Y-m-d')));  ;?></span></p>
                <p class="info">Instructor: <span><?php echo $linkdetails['class_instructor'];?></span></p>
            </div>
            <div class="col float-right text-right">
                <img src="https://www.barefoot-trading.com/wp-content/uploads/2019/05/Barefoot-logo-132x155.png" style="max-width: 100px;" />
            </div>
        </div>
        <div class="row mt-4 mb-5">
            <div class="col pt-3 pb-3" id="disclaimer-content">    
                <p>If you are registering to take part in one of the classes being held at Eggs & Soldiers, you must confirm that:</p>
                <ol>
                <li>You are of legal consenting adult age in the UAE.</li>
                <li>You understand and accept that Eggs and Soldiers is the intermediary of the event and does not assume any responsibility for any death, injuries sustained or loss or damage to any of your possessions or goods (e.g.: money, vehicles, jewellery, clothing, cameras etc) whatsoever and howsoever their loss or damage is caused; and</li>
                <li>3. You agree to all social distancing, government directives and other requirements related to COVID19 during this time.</li>
                <li>4. You will notify us if you wish to not be included in any photos or videos taken during the class. These images/videos will only be used for Eggs & Soldiers marketing material</li>
                </ol>
                <p>I, hereby agree to indemnify Eggs and Soldiers and the event organizers for any and all the claims, loss and expenses of any sort arising by reason of my breach of the above conditions, unauthorised acts or otherwise. Any breach of these conditions or failure by me to obey the lawful directions of Eggs and Soldiers or the classroom instructors hereby entitles Eggs and Soldiers or the class organizers to terminate my participation in an event forthwith without any refund of fees or any compensation of any sort and to disqualify me automatically. </p>
            </div>
        </div>
        
        <hr>
        <?php //print_r($linkdetails);?>
        <form action="" method="post" class="form-horizontal" id="disclaimerForm" autocomplete="off">
                <input type="hidden" value="<?php echo $linkdetails['clink_key'];?>" class="form-control" name="key" id="key"/>
                <div class="row mt-4">
                    <div class="col-12">
                        <div class="form-group">
                            <table class="table table-bordered" id="att_table">
                                <thead>
                                    <tr>
                                        <td>#</td>
                                        <td>Name</td>
                                        <td>Email</td>
                                        <td><a href="javascript:void(0)" class="btn btn-success btn-sm float-right" id="att_add">+</a></td>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td><input type="text" class="form-control" name="att_name[]"/></td>
                                        <td><input type="email" class="form-control" name="att_email[]"/></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>  
                        <div class="form-group form-check pt-2">
                            <input type="checkbox" class="form-check-input" id="acceptTerms">
                            <label class="form-check-label" for="acceptTerms">By completing this form, you agree to have your details added to the Eggs & Soldiers database where we may send you relevant offers and updates. </label>
                        </div>
                    </div>
                </div>
                

                <hr>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-12 text-center">
                        <div id="dynamic_message">
                        </div>
                        <button class="btn btn-default btn-block" type="submit" id="disclaimerFormSubmit">SAVE</button>
                    </div>
                </div>  
        </form>
                <div class="row mt-5">
                    <div class="col-lg-12 col-sm-12 col-12 text-center footer-content">
                        <p>Barefoot General Trading LLC, Reg. #827395</p>
                        <p>G1-1-2, Times Square Center, Al Quoz Industrial First, Dubai, UAE</p>
                    </div>
                </div>  
    </div>
</div>  
<style>
    body{
        font-family: Roboto Slab,sans-serif;
        background: #ff980012;
        margin-top: 50px;
        margin-bottom: 50px;
    }
.main-section{
	padding: 50px 30px;
	/*background:#f1f1f1; #B8850B*/
    background:#fff;
}
.border {
    border: 2px solid #dee2e6!important;
}
.custom-file-control::after {
  content: "Choose file...";
}
.custom-file-control::before {
  content: "Browse";
}
#att_table{
    background-color: #fff !important;   
}
#disclaimerFormSubmit{
    /*background-color: #ed1c24 !important;   
    border: none !important;   */
    background-color: #8e8e8e;
    color: white;
    font-weight: 600;
}
.info{
    margin-bottom: 10px; 
}
.info span{ color: #f58220; }
#disclaimer-content{
    max-height: 200px;
    overflow: scroll;
    border: 2px solid #f5f5f5;
}
#disclaimer-content p, #disclaimer-content ol li{
    text-align: justify; color: #666666; 
}
.footer-content p { margin-bottom: 5px; color: #666666;  }
/* responsive */
@media screen and (min-width: 992px) {
    .main-section{
	    padding-left: 50px;
        padding-right: 50px;
    }
}
.error{
    color: red;
    font-size: 12px;
    font-weight: 600;
}
</style>
<script>
    jQuery(document).ready(function () {
        function updateIndex() { 
            jQuery("#att_table tbody tr").each(function(){
                jQuery( this ).find( "td" ).first().html( $(this).index() + 1 );
            });
        }
        function make_row(){
            var html = '<tr><td>1</td>'
            +'<td><input type="text" class="form-control" name="att_name[]"/></td>'
            +'<td><input type="email" class="form-control" name="att_email[]"/></td>'
            +'<td><a href="javascript:void(0)" class="btn btn-danger btn-sm float-right att_delete" >-</a></td>'
            +'</tr>';
            return html;
        }
        //add
        jQuery(document).on("click", "#att_add", function (event) {
            event.preventDefault();
            $("#att_table tbody").append(make_row());
            updateIndex();
        });
        //Delete
        jQuery(document).on("click", ".att_delete", function (event) {
            event.preventDefault();
            $(this).closest('tr').remove();
            updateIndex();
        });

        //validation
        jQuery("#disclaimerForm").validate({
			rules: {
				att_name: "required",
				att_email: "required"
			}
		});
		
		//on submission
		jQuery(document).on("submit", "#disclaimerForm", function (event) {
			event.preventDefault();
            let isChecked = $('#acceptTerms').is(':checked');
            if(isChecked == true){
                if(jQuery("#disclaimerForm").valid() == true){
                    //loading spinner
                    run_waitMe('#disclaimerForm','bounce');
                    var key = jQuery('#key').val();
                    var name = $("input[name='att_name[]']").map(function(){return $(this).val();}).get();
                    var email =  $("input[name='att_email[]']").map(function(){return $(this).val();}).get();
                    jQuery.ajax({
                        url: BASE_URL+'disclaimersave',
                        type: 'post',
                        dataType: 'json',
                        data: { att_name:name, att_email: email, key: key} ,
                        success: function(data) {
                            //close spinner
                            jQuery('#disclaimerForm').waitMe('hide');
                            if(data.status == 'success'){
                                //jQuery.notify({message: data.message},{type: "success"});
                                notify_new(data.message,'success');
                                setTimeout(function(){
                                    window.location.href = BASE_URL+data.url;
                                }, 5000);
                            }else{
                                //jQuery.notify({message: data.message},{type: 'danger'});
                                notify_new(data.message,'danger');
                                if (data.validation_errors != 0){
                                    jQuery.each(data.validation_errors, function(index, item) {
                                        jQuery.notify({message: item},{type: "danger"});
                                    });     
                                }
                            }
                        }
                    });
                }else{
                    //jQuery.notify({message: 'Fill all required fields.'},{type: 'danger'});
                    notify_new('Fill all required fields.','danger');
                }
            }else{
                //jQuery.notify({message: 'Plaese tick the checkbox for proceed further!'},{type: 'danger'});
                notify_new('Plaese tick the checkbox for proceed further!','danger');
            }
			
		});

        function notify_new(message,type){
            jQuery('#dynamic_message').html('');
            var html = '<div style="margin-bottom: 25px;" class="alert alert-'+type+'">'
            +message
            +'</div>';
            jQuery('#dynamic_message').html(html);
        }
    });
</script>