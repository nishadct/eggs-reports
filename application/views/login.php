<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="sufee-login d-flex align-content-center flex-wrap">
    <div class="container">
        <div class="login-content">
            <div class="login-logo">
                <a href="index.html">
                    <img class="align-content" src="<?php echo base_url('assets/');?>images/ct_logo.png" alt="">
                </a>
            </div>
            <div class="login-form" method="post" action="#">
                <form id="formLogin">
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" class="form-control" placeholder="Email" name="email" id="email">
                    </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                    </div>
                        <div class="checkbox">
                            <label>
                        <input type="checkbox"> Remember Me
                    </label>
                    <label class="pull-right">
                        <!--<a href="#">Forgotten Password?</a>-->
                    </label>

                        </div>
                        <button type="submit" class="btn btn-success btn-flat m-b-30 m-t-30">Sign in</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
jQuery(document).ready(function () {
		//validating and submit
		jQuery("#formLogin").validate({
			rules: {
				password: "required",
				email: {
					required: true,
					email: true
				}
			},
			messages: {
				email: "Please enter a valid email address",
				password: "Please enter your password"
			}
		});
		
		//on submission
		jQuery(document).on("submit", "#formLogin", function (event) {
			event.preventDefault();
			if(jQuery("#formLogin").valid() == true){
				//loading spinner
				run_waitMe('#formLogin','bounce');
				var email = jQuery('#email').val();
				var password = jQuery('#password').val();
				jQuery.ajax({
					url: BASE_URL+'Login',
					type: 'post',
					dataType: 'json',
					//data: $(this).serialize(),
					data: { email: email, password: password} ,
					success: function(data) {
						//close spinner
						jQuery('#formLogin').waitMe('hide');
						if(data.status == 'success'){
							jQuery.notify({message: data.message},{type: "success"});
							window.location.href = BASE_URL+data.url;
						}else{
							jQuery.notify({message: data.message},{type: 'danger'});
						}
					}
				});
			}else{
				jQuery.notify({message: 'Fill all required fields.'},{type: 'danger'});
			}
		});
	});
</script>