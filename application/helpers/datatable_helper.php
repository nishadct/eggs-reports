<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
*  edit_column callback function in Codeigniter (Ignited Datatables)
*
* Grabs a value from the edit_column field for the specified field so you can
* return the desired value.  
*
* @access   public
* @return   mixed
*/

if ( ! function_exists('woo_product_exist_status'))
{
    function woo_product_exist_status($status = '')
    {
        return ($status == 1) ? '<span class="badge badge-success">Synced</span>' : '<span class="badge badge-danger">Not Synced</span>';
    }   
}

if ( ! function_exists('table_counter'))
{
    function datatable_counter($primary_key,$prepend = '')
    {
		//return 0;
		if($prepend != ''){
			$session_variable = $prepend.'_dt_counter';
			$CI = &get_instance();
			//$sess = $this->session->has_userdata('user_id');
			if($prepend != '' && $CI->session->has_userdata($session_variable)) {
				$CI->session->$session_variable = $CI->session->$session_variable+1;
				return $CI->session->userdata($session_variable);
			}else{
				return 0;
			}
		}else
		return 0;
    }   
}