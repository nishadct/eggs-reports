<?php
class Classes_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	//create classes
	public function _create($data){
		$return = ['status'=>false, 'reason'=>''];
		$this->db->insert('ens_classes',$data);
		$insert_id = $this->db->insert_id();
		$return = ['status'=>true, 'id'=>$insert_id];
		return $return;
	}

	public function _get_classes(){
		$this->db->select('*');
		$this->db->from('ens_classes');
		$this->db->where(['class_status'=>'a']);
		$query = $this->db->get();
		return $res = $query->result_array();
	}

	//create class link
	public function _create_link($data){
		$return = ['status'=>false, 'reason'=>''];
		$this->db->insert('ens_classlinks',$data);
		$insert_id = $this->db->insert_id();
		$return = ['status'=>true, 'id'=>$insert_id];
		return $return;
	}

	public function _get_classlink($key){
		$this->db->select('*');
		$this->db->from('ens_classlinks links');
		$this->db->join('ens_classes c','links.clink_class = c.class_id','left');
		$this->db->where(['clink_key'=>$key]);
		$query = $this->db->get();
		return $res = $query->row_array();
	}

	public function _generate_class_link(){
		$key = random_string('numeric', 5);
		//$key = '333';
		while ( $this->_count_link_key($key) ) {
			$key = random_string('numeric', 5);
		}
		return $key;
	}

	public function _count_link_key($key){
		$this->db->where('clink_key',$key);
		return $this->db->count_all_results('ens_classlinks');
	}

	public function _create_attendees($data){
		$return = ['status'=>false, 'reason'=>''];
		$this->db->insert('ens_class_attendees',$data);
		$insert_id = $this->db->insert_id();
		$return = ['status'=>true, 'id'=>$insert_id];
		return $return;
	}
}
?>