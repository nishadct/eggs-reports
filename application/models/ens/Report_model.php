<?php

class Report_model extends CI_Model {
	
	//classes database
	public function _get_class_database($form_id = 39042)
	{
		// Load database
		$wpdb = $this->load->database('wp_db', TRUE);
		$wpdb->select('*');
		$wpdb->where('form_post_id',$form_id);
		$query = $wpdb->get('wpens_db7_forms');
		$res = $query->result_array();
		return $res;
	}
	
	function getClasses($postData=null,$form_id = [39042,42198]){
		$response = array();
		$wpdb = $this->load->database('wp_db', TRUE);
		## Read value
		$draw = $postData['draw'];
		$start = $postData['start'];
		$rowperpage = $postData['length']; // Rows display per page
		$columnIndex = $postData['order'][0]['column']; // Column index
		$columnName = $postData['columns'][$columnIndex]['data']; // Column name
		$columnSortOrder = $postData['order'][0]['dir']; // asc or desc
		//class title
		$class_title = isset($postData['class_title']) ? $postData['class_title'] : '';
		/*$searchValue = $postData['search']['value']; // Search value

		## Search 
		$searchQuery = "";
		if($searchValue != ''){
		$searchQuery = " (emp_name like '%".$searchValue."%' or email like '%".$searchValue."%' or city like'%".$searchValue."%' ) ";
		}*/

		## Total number of records without filtering
		$wpdb->select('count(*) as allcount');
		//$wpdb->where('form_post_id',$form_id);
		$wpdb->where_in('form_post_id', $form_id);
		$records = $wpdb->get('wpens_db7_forms')->result();
		$totalRecords = $records[0]->allcount;

		## Total number of record with filtering
		$wpdb->select('count(*) as allcount');
		/*if($searchQuery != '')
		$this->db->where($searchQuery);*/
		//$wpdb->where('form_post_id',$form_id);
		$wpdb->where_in('form_post_id', $form_id);
		$records = $wpdb->get('wpens_db7_forms')->result();
		$totalRecordwithFilter = $records[0]->allcount;

		## Fetch records
		$this->db->select('*');
		/*if($searchQuery != '')
		$this->db->where($searchQuery);*/
		//$wpdb->where('form_post_id',$form_id);
		$wpdb->where_in('form_post_id', $form_id);
		$wpdb->order_by($columnName, $columnSortOrder);
		if( $rowperpage > 0) $wpdb->limit($rowperpage, $start);
		$records = $wpdb->get('wpens_db7_forms')->result_array();
		//echo '<pre>';print_r($records);die();
		$data = array();
		foreach($records as $class){
			$full_data = unserialize($class['form_value']);
			//echo '<pre>';print_r($full_data);
			//die();
			if(!empty($full_data)){
				if(!empty($class_title)){
					if($class_title != $full_data['course-name']) { continue; }
				}
				$data[] = array( 
					"form_date"=>$class['form_date'],
					"name"=>$full_data['course-user-name'],
					"email"=>$full_data['course-user-email'],
					"telephone"=>$full_data['course-user-telephone'],
					"course-name"=>$full_data['course-name'],
					"course-date"=>isset($full_data['course-date']) ? $full_data['course-date'] : 'Nil',
					"course-provider-email"=>isset($full_data['course-provider-email']) ? $full_data['course-provider-email'] : 'Nil',
					"course-people-count"=>isset($full_data['course-people-count']) ? $full_data['course-people-count'] : 'NA',
					"course_url"=>$full_data['course-url'],
					"course-page"=>$full_data['referer-page']
				); 
			}
		}
		## Response
		//update the totalRecordwithFilter if class title there
		if(!empty($class_title)) $totalRecordwithFilter = count($data);
		$response = array(
		"draw" => intval($draw),
		"iTotalRecords" => $totalRecords,
		"iTotalDisplayRecords" => $totalRecordwithFilter,
		"aaData" => $data
		);

		return $response; 
	}

	//Product Report
	public function _get_product_report($report_type = 'full')
	{
        //default where
        $where = "p.post_type in('product', 'product_variation') AND p.post_status = 'publish'";
		if($report_type == 'oos'){
            $where .= " AND pl.stock_quantity <= 0"; 
        }else if($report_type == 'img'){
			$where .= " AND am.meta_value IS NULL"; 
		}
        // Load database
		$wpdb = $this->load->database('wp_db', TRUE);
		//MAX(CASE WHEN pm1.meta_key = '_sku' then pm1.meta_value ELSE NULL END) as sku, 
		if($report_type == 'img'){
			$sql ="SELECT 
			t.slug as type,
			p.ID product_id,
			p.post_title as product_name,
			p.post_status as product_status,
			pl.sku as product_sku,
			pl.stock_quantity as product_stock,
			am.meta_value as img
			FROM wpens_posts p 
			LEFT JOIN wpens_wc_product_meta_lookup pl ON pl.product_id = p.ID
			LEFT JOIN wpens_term_relationships AS tr ON tr.object_id = p.ID
			LEFT JOIN wpens_term_taxonomy AS tt ON tt.taxonomy = 'product_type' AND tt.term_taxonomy_id = tr.term_taxonomy_id 
			LEFT JOIN wpens_terms AS t ON t.term_id = tt.term_id
			LEFT JOIN wpens_postmeta pm ON pm.post_id = p.ID AND pm.meta_key = '_thumbnail_id'
			LEFT JOIN wpens_postmeta am ON am.post_id = pm.meta_value AND am.meta_key = '_wp_attached_file'
			WHERE $where 
			GROUP BY p.ID";
		}else{
			$sql="SELECT 
			t.slug as type,
			p.ID product_id,
			p.post_title as product_name,
			p.post_status as product_status,
			pl.sku as product_sku,
			pl.stock_quantity as product_stock,
			MAX(CASE WHEN pm1.meta_key = '_regular_price' then pm1.meta_value ELSE NULL END) as regular_price,
			MAX(CASE WHEN pm1.meta_key = '_sale_price' then pm1.meta_value ELSE NULL END) as sale_price
			FROM wpens_posts p 
			LEFT JOIN wpens_wc_product_meta_lookup pl ON pl.product_id = p.ID
			LEFT JOIN wpens_postmeta pm1 ON pm1.post_id = p.ID
			LEFT JOIN wpens_term_relationships AS tr ON tr.object_id = p.ID
			LEFT JOIN wpens_term_taxonomy AS tt ON tt.taxonomy = 'product_type' AND tt.term_taxonomy_id = tr.term_taxonomy_id 
			LEFT JOIN wpens_terms AS t ON t.term_id = tt.term_id
			WHERE $where 
			GROUP BY p.ID";  
		}  
    	$query = $wpdb->query($sql);
    	return $query->result_array();
	}
}
?>