<?php
class Users_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	
	
	//chck login
	public function login($email, $password){
		$query = $this->db->get_where('users', array('user_email'=>$email, 'user_password'=>md5($password)));
		return $query->row_array();
	}
	//create users
	public function create_users($data){
		$return = ['status'=>false, 'reason'=>''];
		$query = $this->db->get_where('users', array('user_email'=>$data['user_email']));
		$res = $query->row_array();
		if(!empty($res )){
			$return = ['status'=>false, 'reason'=>'Email already exists!'];
		}else{
			$this->db->insert('users',$data);
			$insert_id = $this->db->insert_id();
			$return = ['status'=>true, 'id'=>$insert_id];
		}
		return $return;
	}
	//get all users
	public function get_users($status = ''){
		$this->db->select('*');
		$this->db->from('users');
		$where = array();
		if($status != ''){
			$where['user_status'] = 'a';
		}
		if(!empty($where)){
			$this->db->where($where);
		}
		$this->db->order_by('user_on','DESC');
		$query = $this->db->get();
		return $res = $query->result_array();
	}
	
	public function make_select_array($two_dimension_array){
		$selectArray = array();
		foreach($two_dimension_array as $row){
			$selectArray[$row['id']] = $row['user_firstname'].' '.$row['user_lastname'];
		}
		return $selectArray;
	}
	
	//create a login for checking
	public function create_first_login(){
		$data = array(
			'user_firstname' => 'Nishad',
			'user_email'  => 'nishad@chabowskitrading.com',
			'user_password'  => md5('test'),
			'user_type'	=>1,
			'user_status'=>'a',
			'user_on'=>date('Y-m-d H;i:s')
		);
		$this->db->insert('users', $data);
	}

}
?>