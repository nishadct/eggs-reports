<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model('users_model');
	}

	public function index()
	{
		if ($this->session->sessLoggedIn === TRUE)
		{
			 redirect('/Dashboard', 'refresh');
		}
		$output = array('status' => 'error','message'=>"",'validation_errors'=>array());
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			//$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if ($this->form_validation->run() == FALSE) {
				$output['validation_errors'] = $this->form_validation->error_array();
				$output['message'] = "Validations fials..";
			}else{
				$email = $this->security->xss_clean($this->input->post('email'));
				$password = $this->security->xss_clean($this->input->post('password'));
				$result = $this->users_model->login($email,$password);
				if($result){
					if($result['user_status'] == 'i'){
						$output['message'] = 'Your are not an active user now. Please contact administrator.';
					}else{
						//success, set session
						$newdata = array(
							'sessUserId'  	=> $result['id'],
							'sessUserEmail'	=> $result['user_email'],
							'sessUserName'	=> $result['user_firstname'],
							'sessUserType' 	=> $result['user_type'],
							'sessLoggedIn' 	=> TRUE
						);
						$this->session->set_userdata($newdata);
						$output['status'] = 'success';
						$output['message'] = 'Logging in. Please wait...';
						$output['url'] = '/Dashboard';
						//redirect('/Dashboard', 'refresh');
					}
				}else{
					$output['message'] = 'Credentials are wrong.. Please try with correct one.';
				}
			}
			echo json_encode($output); exit();
		}else{
			$data = array();
			$this->template->set('title', 'Login');
			$this->template->load('auth_layout', 'contents' , 'login', $data);
		}
	}
	
	public function logout() {
		$this->session->sess_destroy();
		redirect('/Login', 'refresh');
    }
	
	public function mylogin() {
        $this->users_model->create_first_login();
		redirect('Login');
    }
}
