<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		//$this->load->model('users_model');
		/*if ($this->session->sessLoggedIn != TRUE)
		{
			 redirect('/Login', 'refresh');
		}*/
		if(! $this->session->sessLoggedIn) {
            redirect('/Login');
        }
	}

	public function index()
	{
		$data = array();
		$this->template->set('title', 'Home');
		$this->template->load('default_layout', 'contents' , 'dashboard', $data);
	}
}
