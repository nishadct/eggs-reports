<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendsync extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
   		parent::__construct();
		$this->load->model('vendwoosync_model');
		include APPPATH . 'third_party/VendAPI/VendAPI.php';
	}
	
	public function index()
	{
		$this->load->view('layout');
	}
	
	//connect Vend
	public function connectVend(){
		$token_type = VEND_TOKEN_TYPE;
		$secret_key = VEND_API_TOKEN;
		return $vend = new VendAPI\VendAPI(VEND_URL,$token_type,$secret_key);
	}
	
	// a small function to make loop through inventory array/ obj and return count
	public function getInventoryByProcessingObject($inventoryObj)
	{
		$inventory_count = 0;
		$inventoryToUpdate = array(
			'product_stock_count_warehouse'=>0,
			'product_stock_count_shop'=>0,
			'product_stock_count_suppliers'=>0
		);
		//get outlets
		$outlets = $this->vendwoosync_model->_get_outlet_ids();
		if(!empty($inventoryObj)) {
			foreach($inventoryObj as $payload){
				//$inventory_count = $inventory_count + $payload->count;
				$outlet_id = $payload->outlet_id;
				if($outlet_id == $outlets['warehouse']){//if warehouse
					$inventoryToUpdate['product_stock_count_warehouse'] = $payload->count;
				}else if($outlet_id == $outlets['shop']){//if shop
					$inventoryToUpdate['product_stock_count_shop'] = $payload->count;
				}else if($outlet_id == $outlets['suppliers']){//if suppliers
					$inventoryToUpdate['product_stock_count_suppliers'] = $payload->count;
				}
			}
			
		}
		return $inventoryToUpdate;
	}
	
	/*getting all products from Vend and add needed details to databse*/
	public function retrieveProductFromVendManual()
	{
		$productsToAdd = $productsToUpdate = array();
		$i = $j = 0;
		// Connectt vend
		$vend = $this->connectVend();
		$vend->automatic_depage = true;
        $productsObj = $vend->getProducts();
		//echo '<pre>';print_r($productsObj);die();
		if(!empty($productsObj->products)){
			//$resp = json_decode($productsObj, true);
			//echo'<pre>'; print_r($productsObj->products); echo'</pre>';die();
			foreach($productsObj->products as $product){
				$productsToAdd = array();
				if(isset($product->inventory)){
					$inventoryToUpdate = $this->getInventoryByProcessingObject($product->inventory);
				}else{
					$inventoryToUpdate = array(
						'product_stock_count_warehouse'=>0,
						'product_stock_count_shop'=>0,
						'product_stock_count_suppliers'=>0
					);
				}
				$productsToAdd = array(
					'vend_product_id' => $product->id,
					'product_sku' => $product->sku,
					'product_title' =>$product->name,
					'product_stock_count_shop'=>$inventoryToUpdate['product_stock_count_shop'],
					'product_stock_count_warehouse'=>$inventoryToUpdate['product_stock_count_warehouse'],
					'product_stock_count_suppliers'=>$inventoryToUpdate['product_stock_count_suppliers'],
					'product_price' => $product->price + $product->tax,
					'vend_product_status' => $product->active,
					'vend_last_sync_time' => date('Y-m-d H:i:s')
				);
				$this->vendwoosync_model->_insert_product($productsToAdd);
				$i++;
			}
			//if( !empty($productsToAdd) || !empty($productsToUpdate) ){
			if( $i > 0 ){
				$this->session->set_flashdata('success_msg', 'Retrieved all products from vend and updated to the database.');
			}else{
				$this->session->set_flashdata('failure_msg', 'Failed. Something went wrong.');
			}
		}else{
			$this->session->set_flashdata('warning_msg', 'No updates from vend.');
		}
	}
	
	/*getting latest updated products only from Vend -- for connecting to cron*/
	public function retrieveLatestUpdatedProductFromVendCron()
	{
		$productsToAdd = $productsToUpdate = array();
		$i = $j = 0;
		$vend = $this->connectVend();
		/*$utctime =  gmdate(DATE_ISO8601, strtotime('2019-02-01 23:59:46'));*/
		//get last sync time from vend
		$lastSynctime = $this->vendwoosync_model->_get_last_vend_price_sync_time();
		$utctime =  gmdate(DATE_ISO8601, strtotime($lastSynctime));
		$utctime = str_replace('+0000', "", $utctime);
		//$utctime = '2019-04-28T00:00:00';
		//echo '<pre>';print_r($utctime);
		$vend->automatic_depage = true;
		$productsObj = $vend->getProducts(array('since'=>$utctime));
		$data['cron'] = 'retrieveLatestUpdatedProductFromVendCron'; 
		$data['date'] = date('Y-m-d H:i:s'); 
		$this->vendwoosync_model->_test(serialize($data));
		//echo '<pre>';print_r($productsObj);
		if(!empty($productsObj->products)){
			foreach($productsObj->products as $product){
				//first, check the product is alreday availabale in database:
				$exist = 0;
				$existCheck = $this->vendwoosync_model->_check_product_is_already_synced($product->id,'full');
				if(isset($product->inventory)){
					$inventoryToUpdate = $this->getInventoryByProcessingObject($product->inventory);
				}else{
					$inventoryToUpdate = array(
						'product_stock_count_warehouse'=>0,
						'product_stock_count_shop'=>0,
						'product_stock_count_suppliers'=>0
					);
				}
				$newPrice = round($product->price + $product->tax);
				if(!empty($existCheck)){//need to update the product details
					$exist = $existCheck['id'];
					$existPrice = $existCheck['product_price'];
					if($existPrice != $newPrice |$product->sku != $existCheck['product_sku']){//if any changes in price or sku
						$productsToUpdate[$j] = array(
							//'id'=>$exist,
							'product_sku' => $product->sku,
							'product_title' =>$product->name,
							//'product_stock_count' => $product->track_inventory == true && isset($product->inventory) ? $this->getInventoryByProcessingObject($product->inventory):0,
							//'product_stock_count_shop'=>$inventoryToUpdate['product_stock_count_shop'],
							//'product_stock_count_warehouse'=>$inventoryToUpdate['product_stock_count_warehouse'],
							'product_price' => $newPrice,
							'vend_product_status' => $product->active,
							'vend_last_sync_time' => date('Y-m-d H:i:s'),
							'woo_updated_status'  => 0
						);
						$this->vendwoosync_model->_update_product($productsToUpdate[$j],$exist);
						$j++;
					}
				}else{//need to add as new 
					$productsToAdd[$i] = array(
						'vend_product_id' => $product->id,
						'product_sku' => $product->sku,
						'product_title' =>$product->name,
						'product_stock_count_shop'=>$inventoryToUpdate['product_stock_count_shop'],
						'product_stock_count_warehouse'=>$inventoryToUpdate['product_stock_count_warehouse'],
						'product_stock_count_suppliers'=>$inventoryToUpdate['product_stock_count_suppliers'],
						'product_price' => $newPrice,
						'vend_product_status' => $product->active,
						'vend_last_sync_time' => date('Y-m-d H:i:s')
					);
					$this->vendwoosync_model->_insert_product($productsToAdd[$i]);
					$i++;
				}
			}
			//process arrays
			//if( !empty($productsToAdd) || !empty($productsToUpdate) ){
			if( $i > 0 || $j > 0 ){
				//if( !empty($productsToAdd) ) $this->vendwoosync_model->_insert_product_to_sync($productsToAdd);//add
				//if( !empty($productsToUpdate) ) $this->vendwoosync_model->_update_product_to_sync($productsToUpdate);//update
				$this->vendwoosync_model->_update_vend_last_price_sync_time();
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	/*same functionality of retrieveLatestUpdatedProductFromVendCron(), but for manual only*/
	public function retrieveLatestUpdatedProductFromVendManual()
	{
		$productsToAdd = $productsToUpdate = array();
		$i = $j = 0;
		$vend = $this->connectVend();
		//get last sync time from vend
		$lastSynctime = $this->vendwoosync_model->_get_last_vend_sync_time();
		$utctime =  gmdate(DATE_ISO8601, strtotime($lastSynctime));
		$utctime = str_replace('+0000', "", $utctime);
		$vend->automatic_depage = true;
		$productsObj = $vend->getProducts(array('since'=>$utctime));
		//echo '<pre>';print_r($productsObj);die();
		if(!empty($productsObj->products)){
			foreach($productsObj->products as $product){
				//first, check the product is alreday availabale in database:
				$exist = $this->vendwoosync_model->_check_product_is_already_synced($product->id);
				if($exist > 0){//need to update the product details
					$productsToUpdate[$j] = array(
						//'id'=>$exist,
						'product_sku' => $product->sku,
						'product_title' =>$product->name,
						'product_stock_count' => $product->track_inventory == true && isset($product->inventory) ? $this->getInventoryByProcessingObject($product->inventory):0,
						'product_price' => $product->price + $product->tax,
						'vend_product_status' => $product->active,
						'vend_last_sync_time' => date('Y-m-d H:i:s')
					);
					$this->vendwoosync_model->_update_product($productsToUpdate[$j],$exist);
					$j++;
				}else{//need to add as new 
					$productsToAdd[$i] = array(
						'vend_product_id' => $product->id,
						'product_sku' => $product->sku,
						'product_title' =>$product->name,
						'product_stock_count' => $product->track_inventory == true && isset($product->inventory) ? $this->getInventoryByProcessingObject($product->inventory):0,
						'product_price' => $product->price + $product->tax,
						'vend_product_status' => $product->active,
						'vend_last_sync_time' => date('Y-m-d H:i:s')
					);
					$this->vendwoosync_model->_insert_product($productsToAdd[$i]);
					$i++;
				}
			}
			//process arrays
			if( $i > 0 || $j > 0 ){
				//if( !empty($productsToAdd) ) $this->vendwoosync_model->_insert_product_to_sync($productsToAdd);//add
				//if( !empty($productsToUpdate) ) $this->vendwoosync_model->_update_product_to_sync($productsToUpdate);//update
				$this->vendwoosync_model->_update_vend_last_sync_time();
				$this->session->set_flashdata('success_msg', 'Retrieved all latest changes from vend and updated to the database.');
			}else{
				$this->session->set_flashdata('failure_msg', 'Failed. Something went wrong.');
			}
		}else{
			$this->session->set_flashdata('warning_msg', 'No updates from vend.');
		}
		redirect('/Vendsync', 'refresh');
	}
	
}
