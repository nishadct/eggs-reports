<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ensreports extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		if(! $this->session->sessLoggedIn) {
            redirect('/Login');
        }
		$this->load->library('datatables');
		$this->load->helper('Datatable_helper');
		$this->load->model('ens/report_model');
	}

	public function waitlist()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			//$this->session->product_dt_counter = 0;
			//get product website status
			/*$product_website_status = $this->input->post('product_website_status');
			$product_stock_status = $this->input->post('product_stock_status');
			$product_brand = $this->input->post('product_brand');*/
			$this->datatables->set_database('wp_db');
			$this->datatables->select('pw_id,pw_sku,pw_title,pw_email,pw_on');
			$this->datatables->from('wpens_custom_table_for_product_waitlist');
			//$this->datatables->join('ens_vend_brands', 'brand_id = evb_Id', 'left');
			/*if($product_website_status != '') $this->datatables->where(array('woo_product_exist'=>$product_website_status));
			if($product_stock_status != ''){
				$product_stock_status == 'instock' ? $this->datatables->where('(product_stock_count_shop > 0 OR product_stock_count_warehouse > 0 OR product_stock_count_suppliers > 0)'): $this->datatables->where('(product_stock_count_shop <= 0 OR product_stock_count_warehouse <= 0 OR product_stock_count_suppliers <= 0)');
			}
			if($product_brand != '') $this->datatables->where(array('brand_id'=>$product_brand));*/
			//add column
			$this->datatables->add_column('view', '<a href="javascript:void(0);" class="edit_record btn btn-info" data-code="$1">View</a>','pw_id');
			$this->datatables->edit_column('id','$1','$this->datatable_counter(id,"product")');
      		echo $this->datatables->generate();
		}else{
			$data = [];
			$this->template->set('title', 'EnS Products');
			$this->template->load('default_layout', 'contents' , 'ens/waitlist', $data);
		}
	}

	//not using
	/*public function classesworkshops()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$classes = $this->report_model->_get_class_database();
			$data = array();
			$i = 0;
			foreach($classes as $class){
			   $data[$i]['on'] = $class['form_date'];
			   $full_data = unserialize($class['form_value']);
			   if(!empty($full_data)){
				   $data[$i]['name'] = $full_data['course-user-name'];
				   $data[$i]['email'] = $full_data['course-user-email'];
				   $data[$i]['telephone'] = $full_data['course-user-telephone'];
				   $data[$i]['course-name'] = $full_data['course-name'];
				   $data[$i]['course-date'] = isset($full_data['course-date']) ? $full_data['course-date'] : '';
				   $data[$i]['course-url'] = $full_data['course-url'];
				   $data[$i]['referer-page'] = $full_data['referer-page'];
			   }
			   $i++;
			}
			echo json_encode( ['data' => $data] );
		}else{
			$this->template->set('title', 'EnS Products');
			$this->template->load('default_layout', 'contents' , 'ens/classes', $data);
		}
	}*/
	//classes interests
	public function classesinterests()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			// POST data
			$postData = $this->input->post();

			// Get data
			$data = $this->report_model->getClasses($postData,[42198]);

			echo json_encode($data);
		}else{
			$data = [];
			$this->template->set('title', 'EnS Products');
			$this->template->load('default_layout', 'contents' , 'ens/classes-interest', $data);
		}
	}
	//classes
	public function classes()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			// POST data
			$postData = $this->input->post();

			// Get data
			$data = $this->report_model->getClasses($postData,[39042]);

			echo json_encode($data);
		}else{
			$data = [
				'classes' => [
					'Cloth Nappies Uncovered',
					'Car Seat Safety: Need-to-Knows',
					'Safe Sleep: The First Years'
				]
			];
			$this->template->set('title', 'EnS Products');
			$this->template->load('default_layout', 'contents' , 'ens/classes', $data);
		}
	}
	//classes
	public function externalclasses()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			// POST data
			$postData = $this->input->post();

			// Get data
			$data = $this->report_model->getClasses($postData,[45729]);

			echo json_encode($data);
		}else{
			$data = [];
			$this->template->set('title', 'EnS Products');
			$this->template->load('default_layout', 'contents' , 'ens/external_classes', $data);
		}
	}
	//Product reports: starting
	public function product_reports(){
		$data = [];
		$this->template->set('title', 'EnS Products Reports');
		$this->template->load('default_layout', 'contents' , 'ens/product_reports', $data);
	}
	public function export_products_report(){
        $report_type = $this->uri->segment('3');
        //print_r($report_type);die();
		if($report_type == 'img'){
			$filename = 'Image Missing Products - '.date('Y-m-d');
			$header = ['Product Id','Product Name','SKU','Status','Stock', 'Product Type'];
			$data[] = $header;
			//get rows from db
			$results = $this->report_model->_get_product_report($report_type);
			foreach($results as $product){
				if($product['type'] != 'variable'){//skip parent products
					$data[]	= [
						$product['product_id'],
						$product['product_name'],
						$product['product_sku'],
						$product['product_status'],
						$product['product_stock'],
						$product['type'] == NULL ? 'Variation' : $product['type']
					];
				}
			}
		}else{
			$filename = "Products $report_type - ".date('Y-m-d');
			$header = ['Product Id','Product Name','SKU','Status','Stock', 'Regular Price','Sale Price'];
			$data[] = $header;
			//get rows from db
			$results = $this->report_model->_get_product_report($report_type);
			foreach($results as $product){
				if($product['type'] != 'variable'){//skip parent products
					$data[]	= [
						$product['product_id'],
						$product['product_name'],
						$product['product_sku'],
						$product['product_status'],
						$product['product_stock'],
						$product['regular_price'],
						$product['sale_price']
					];
				}
			}
		}
		//print_r($results);die();
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=\"$filename".".csv\"");
		header("Pragma: no-cache");
		header("Expires: 0");

		$handle = fopen('php://output', 'w');

		foreach ($data as $data_array) {
			fputcsv($handle, $data_array);
		}
		fclose($handle);
		exit;
	}
}
