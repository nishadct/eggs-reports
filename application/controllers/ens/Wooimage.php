<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Automattic\WooCommerce\Client;
use Automattic\WooCommerce\HttpClient\HttpClientException;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Wooimage extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
   		parent::__construct();
		$this->load->model('Wooimage_model');
	}
	//connect woo store
	public function connectStore(){
		$consumer_key = 'ck_7f3d1f525d660640af6929b0c0c3501ec78af8b4';
		$consumer_secret = 'cs_fdb398bf60584b71b97421d4ad6795c2039d935d';
		return $woocommerce = new Client('https://www.eggsnsoldiers.com/',$consumer_key,$consumer_secret,array('wp_api' => true, 'version' => 'wc/v3'));
	}

	//update latest changes to woo through cron
	public function connectImage(){
		//get all cahned values from database
		$products = $this->Wooimage_model->get_entries_of_image_table();
		
		if(!empty($products)) {
			$woocommerce = $this->connectStore();
			
			foreach($products as $product){
				$data = [
					'images' => [['src'=>$product['image_link']]],
					'status'=>'publish'
				];
				//echo '<pre>';print_r($data);die();
				//update to woo
				try {
					$yes = $woocommerce->put('products/'.$product['product_id'],$data);
					
					$this->Wooimage_model->_update_image_status(['update_status'=>1],$product['id']);
				}catch(HttpClientException $e) {
					$message = $e->getMessage();
					//print_r($message);die();
					$this->Wooimage_model->_update_image_status(['update_status'=>2,'message'=>$message ],$product['id']);
				}
			}
		}
	}
	
	
	
	public function updateBookwormFromExcel(){
		$dataToInsert = [];
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		$spreadsheet = $reader->load('uploads/bookworm.xlsx');
		//$highestRow = $spreadsheet->getActiveSheet()->getHighestRow();
		//echo $highestRow;
		$sheetData = $spreadsheet->getActiveSheet()->toArray();
		//echo "<pre>";print_r($sheetData);echo "</pre>";die();
		foreach($sheetData as $data){
			$dataToInsert[] = [
					'product_id' => $data['0'],
					'image_link' => 'https://www.vend.eggsnsoldiers.com/uploads/books/'.$data['1'].'.jpg',
					'update_status'=>'0'
			];
		}
		//insert allto db
		if(!empty($dataToInsert)){
			$res = $this->Wooimage_model->_insert_product_to_table($dataToInsert);
			if($res == true) {
				echo 'Success';
			}else{
				echo 'Failure';
			}
		}else{
			echo 'Empty';
		}
		}

		
}
