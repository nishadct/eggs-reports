<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Handlenewvendaccount extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		if(! $this->session->sessLoggedIn) {
            redirect('/Login');
        }
		$this->load->library('datatables');
		$this->load->helper('Datatable_helper');
		$this->load->model('ens/ens_model','ens_model');
		$this->load->model('vendwoosync_model');
		//$this->load->model('client_model');
		//$this->load->model('users_model');
	}

	
	
	//jus for update brands of Vend products
	public function checkConnection()
	{
		// Get cURL resource
		$curl = curl_init();
		$header = array();
		$header[] = 'Content-type: application/json';
		$header[] = 'Authorization: '.VEND_TOKEN_TYPE.' '.VEND_API_TOKEN;
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => VEND_URL.'/api/2.0/products?page_size=10',
			CURLOPT_HTTPHEADER => $header
		]);
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		
		if(!empty($resp)){
			$count = 0;
			$resp = json_decode($resp, true);
			echo '<pre>';print_r($resp['data']);echo '</pre>';die();
			foreach($resp['data'] as $product){
				//echo'<pre>'; print_r($product); echo'</pre>';
				//echo $product['id'];
				if(!empty($product['brand'])){
					$this->app_model->_update_brand($product['id'],$product['brand']);
					//echo $product['brand']['name'].'</br>';
				}else{
					//echo 'No Brand</br>';
				}
				echo $product['id'].'</br>';
				$count++;
			}
			echo '<br>'.$count;
		}
		
		// Close request to clear up some resources
		curl_close($curl);
	}
	
	
	//update product info for new vend account
	public function updateProductInfo()
	{
		// Get cURL resource
		$curl = curl_init();
		$header = array();
		$header[] = 'Content-type: application/json';
		$header[] = 'Authorization: Bearer KiQSsELLtoGzmrbnO20eR_ZEEh7kt5yCq9BAvqYg';
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'https://eggsnsoldiers.vendhq.com/api/2.0/products?page_size=4000',
			CURLOPT_HTTPHEADER => $header
		]);
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		if(!empty($resp)){
			$count = 0;
			$resp = json_decode($resp, true);
			foreach($resp['data'] as $product){
				//echo'<pre>'; print_r($product); echo'</pre>';
				//echo $product['id'];
				if(!empty($product['brand'])){
					$this->app_model->_update_brand($product['id'],$product['brand']);
					//echo $product['brand']['name'].'</br>';
				}else{
					//echo 'No Brand</br>';
				}
				echo $product['id'].'</br>';
				$count++;
			}
			echo '<br>'.$count;
		}
		
		// Close request to clear up some resources
		curl_close($curl);
	}
	
	public function updateOldsku(){
		$dataToUpdate = array();
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		$spreadsheet = $reader->load('uploads/newvendskuupdate.xlsx');
		$sheetData = $spreadsheet->getActiveSheet()->toArray();
		//echo "<pre>";print_r($sheetData);echo "</pre>";die();
		foreach($sheetData as $data){
			$entryId = $this->ens_model->_get_entry_from_sku($data['0']);
			//echo "<pre>";print_r($entryId.$data['1']);echo "</pre>";die();
			if($entryId > 0) {
				$dataToUpdate[] = array(
					  'id' => $entryId,
					  'product_oldsku' => $data['1']
				);
			}
		}
		//insert allto db
		if(!empty($dataToUpdate)){
			$res = $this->vendwoosync_model->_update_product_to_sync($dataToUpdate);
			if($res == true) {
				echo 'Success';
			}else{
				echo 'Failure';
			}
		}else{
			echo 'Empty';
		}
	}
}
