<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ensfrontend extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->library('encryption');
		$this->load->model('ens/Classes_model');
	}
	public function disclaimer()
	{
		$key = $this->uri->segment('2');
		$linkdetails = $this->Classes_model->_get_classlink($key);
		if(empty($linkdetails)){
			return $this->load->view('404_override'); 
		}else{
			//print_r($linkdetails);
			$data['linkdetails'] = $linkdetails;
			$this->template->set('title', 'Classes');
			$this->template->load('frontend_layout', 'contents' , 'frontend/view', $data);
		}
	}

	public function disclaimersave(){
		$output = array('status' => 'error','message'=>"",'validation_errors'=>array());
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->form_validation->set_rules('att_name[]', 'Name', 'trim|required');
			$this->form_validation->set_rules('att_email[]', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('key', 'Status', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$output['validation_errors'] = $this->form_validation->error_array();
				$output['message'] = "Validations fials.. Enter full details please!";
			}else{
				$key = $this->security->xss_clean($this->input->post('key'));
				$att_name = $this->security->xss_clean($this->input->post('att_name'));
				$att_email = $this->security->xss_clean($this->input->post('att_email'));
				$linkdetails = $this->Classes_model->_get_classlink($key);
				if(!empty($linkdetails)){
					$i= 0 ;
					foreach($att_name as $arraykey=>$val){
						$data = [
							'ca_classlinkkey'=>$key,
							'ca_classlinkid'=>$linkdetails['clink_id'],
							'ca_name'=>$att_name[$arraykey],
							'ca_created_on'=>date('Y-m-d H:i:s'),
							'ca_email'=>$att_email[$arraykey]
						];
						$this->Classes_model->_create_attendees($data);
						$i++;
					}
					if($i > 0){
						$output['status'] = 'success';
						$output['message'] = 'Thank you! Details successfully updated!';
						$output['url'] = '/disclaimer/'.$key ;
					}else{
						$output['message'] = 'SOmething went wrong! Please contact administrator.';
					}
				}else{
					$output['message'] = 'Invalid link!';
				}
			}
			echo json_encode($output); exit();
		}else{
			return $this->load->view('404_override'); 
		}
	}
}
