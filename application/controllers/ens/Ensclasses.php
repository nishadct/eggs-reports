<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ensclasses extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		if(! $this->session->sessLoggedIn) {
            redirect('/Login');
        }
		$this->load->library('datatables');
		$this->load->library('encryption');
		$this->load->helper('string');
		$this->load->helper('Datatable_helper');
		$this->load->model('ens/classes_model');
	}
	//classess
	public function manage()
	{
		$output = array('status' => 'error','message'=>"",'validation_errors'=>array());
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->form_validation->set_rules('class_name', 'Class Name', 'trim|required');
			$this->form_validation->set_rules('class_instructor', 'Class Instructor', 'trim|required');
			$this->form_validation->set_rules('class_status', 'Status', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$output['validation_errors'] = $this->form_validation->error_array();
				$output['message'] = "Validations fials..";
			}else{
				$data = [
					'class_name'=>$this->security->xss_clean($this->input->post('class_name')),
					'class_instructor'=>$this->security->xss_clean($this->input->post('class_instructor')),
					'class_status'=>$this->security->xss_clean($this->input->post('class_status')),
					'class_created_on'=>date('Y-m-d H:i:s'),
					'class_created_by'=>$this->session->sessUserId
				];
				$result = $this->classes_model->_create($data);
				if($result['status']== true){
					$output['status'] = 'success';
					$output['message'] = 'Successfully created';
					$output['url'] = '/manageclasses';
				}else{
					$output['message'] = $result['reason'];
				}
			}
			echo json_encode($output); exit();
		}else{
			$data['statuses'] = ['a'=>'Active','i'=>'Inactive'];
			$this->template->set('title', 'Classes');
			$this->template->load('default_layout', 'contents' , 'classes/manage_classes', $data);
		}
	}
	//table view
	public function view()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->datatables->select('class_id ,class_name,class_instructor,class_status,class_created_on');
			$this->datatables->from('ens_classes');
      		echo $this->datatables->generate();
		}else{
			redirect('/manageclasses');
		}
	}
	/* Link area starts here*/
	public function managelinks()
	{
		$output = array('status' => 'error','message'=>"",'validation_errors'=>array());
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->form_validation->set_rules('class_id', 'Class Name', 'trim|required');
			$this->form_validation->set_rules('class_date', 'Class Date', 'trim|required');
			$this->form_validation->set_rules('link_status', 'Status', 'trim|required');
			if ($this->form_validation->run() == FALSE) {
				$output['validation_errors'] = $this->form_validation->error_array();
				$output['message'] = "Validations fials..";
			}else{
				$data = [
					'clink_class'=>$this->security->xss_clean($this->input->post('class_id')),
					'clink_date'=>$this->security->xss_clean( date( 'Y-m-d', strtotime( $this->input->post('class_date') ) ) ),
					'clink_status'=>$this->security->xss_clean($this->input->post('link_status')),
					'clink_key'=>$this->classes_model->_generate_class_link(),
					'clink_created_on'=>date('Y-m-d H:i:s'),
					'clink_created_by'=>$this->session->sessUserId
				];
				$result = $this->classes_model->_create_link($data);
				if($result['status']== true){
					$output['status'] = 'success';
					$output['message'] = 'Successfully created';
					$output['url'] = '/manageclasslinks';
				}else{
					$output['message'] = $result['reason'];
				}
			}
			echo json_encode($output); exit();
		}else{
			//$data['js'] = ['https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js'];
			//$data['css'] = ['https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css'];
			$data['classess'] = $this->classes_model->_get_classes();
			$data['statuses'] = ['a'=>'Active','i'=>'Inactive'];
			$this->template->set('title', 'Classes');
			$this->template->load('default_layout', 'contents' , 'classes/manage_classes_link', $data);
		}
	}
	//table view
	public function linkstable()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->datatables->select('clink_id,class_name,clink_date,class_instructor,clink_status,CONCAT(user_firstname, " ", user_lastname) AS name');
			$this->datatables->select('DATE_FORMAT(`clink_created_on`, \'%d-%m-%Y\') AS `clink_created_on`', false);
			//$this->datatables->select('clink_class');
			$this->datatables->select('clink_key');
			$this->datatables->from('ens_classlinks links');
			$this->datatables->join('ens_classes c','links.clink_class = c.class_id','left');
			$this->datatables->join('users u','u.id = links.clink_created_by','left');
			//$this->datatables->add_column('Actions', btn_edit("admin/posts/edit/$1") . ' ' . btn_delete("admin/posts/delete/$1"), 'clink_id');
			$this->datatables->add_column('Link', 
			'<div class="input-group"><div class="input-group-btn">
				<a href="javascript:void(0)" class="btn btn-info copy_to_clipboard" data-id="$1">
					<i class="fa fa-copy"></i> Copy
				</a>
			</div>
			<input type="text" id="$1" class="form-control" value="'.base_url('disclaimer/$1').'" >
			</div>',
			//'<a href="javascript:void(0)" class="copy_to_clipboard" data-id="$1">Copy</a><input type="text" id="$1" class="form-control" value="'.base_url('disclaimer/$1').'" >', 
			'clink_key');
      		echo $this->datatables->generate();
		}else{
			redirect('/manageclasses');
		}
	}

	//Attendees table
	public function attendesstable()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->datatables->select('ca_id,class_name,class_instructor,clink_date,ca_name,ca_email,ca_created_on');
			$this->datatables->from('ens_class_attendees attendees');
			$this->datatables->join('ens_classlinks links','links.clink_id = attendees.ca_classlinkid','left');
			$this->datatables->join('ens_classes c','links.clink_class = c.class_id ','left');
      		echo $this->datatables->generate();
		}else{
			$data = [];
			$this->template->set('title', 'Classes');
			$this->template->load('default_layout', 'contents' , 'classes/manage_attendees', $data);
		}
	}
}
