<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ensproducts extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		if(! $this->session->sessLoggedIn) {
            redirect('/Login');
        }
		$this->load->library('datatables');
		$this->load->helper('Datatable_helper');
		$this->load->model('app_model');
		$this->load->model('vendwoosync_model');
		//$this->load->model('client_model');
		//$this->load->model('users_model');
	}

	public function index()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->session->product_dt_counter = 0;
			//get product website status
			$product_website_status = $this->input->post('product_website_status');
			$product_stock_status = $this->input->post('product_stock_status');
			$product_brand = $this->input->post('product_brand');
			$this->datatables->select('id,vend_product_id,product_title,product_sku,product_price,(product_stock_count_shop + product_stock_count_warehouse ) as totalStock,evb_VendBrandName,woo_product_exist');
			//$this->datatables->unset_column('id');
			//$count = $count + 1;
			//$this->datatables->add_column('id', $count++, 'id');
            //->add_column('Actions', get_buttons('$1'), 'id')
			$this->datatables->from('ens_vend_sync');
			$this->datatables->join('ens_vend_brands', 'brand_id = evb_Id', 'left');
			$this->datatables->from('ens_vend_sync');
			if($product_website_status != '') $this->datatables->where(array('woo_product_exist'=>$product_website_status));
			if($product_stock_status != ''){
				$product_stock_status == 'instock' ? $this->datatables->where('(product_stock_count_shop > 0 OR product_stock_count_warehouse > 0)'): $this->datatables->where('(product_stock_count_shop <= 0 OR product_stock_count_warehouse <= 0)');
			}
			if($product_brand != '') $this->datatables->where(array('brand_id'=>$product_brand));
			//add column
			//$this->datatables->add_column('view', '<a href="javascript:void(0);" class="edit_record btn btn-info" data-code="$1" data-name="$2" data-price="$3" data-category="$4">Edit</a>  <a href="javascript:void(0);" class="delete_record btn btn-danger" data-code="$1">Delete</a>','id,vend_product_id,product_title,product_sku,product_price,(product_stock_count_shop + product_stock_count_warehouse ) as totalStock,evb_VendBrandName,woo_product_exist');
			//$this->datatables->edit_column('woo_product_exist', '<a href="profiles/edit/$1">$2</a>', 'id, username');
			$this->datatables->edit_column('id','$1','$this->datatable_counter(id,"product")');
			$this->datatables->edit_column('woo_product_exist','$1','$this->woo_product_exist_status(woo_product_exist)');
      		echo $this->datatables->generate();
			//$this->datatables->select('id,vend_product_id,product_sku,product_title,product_price,product_stock_count')
            //->unset_column('id')
            //->from('ens_vend_sync');
        	//echo $this->datatables->generate();
		}else{
			$data['brands'] = $this->app_model->_get_all_brands();
			$this->template->set('title', 'EnS Products');
			$this->template->load('default_layout', 'contents' , 'ens/ensproducts', $data);
		}
	}
	
	public function brands()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->datatables->select('evb_Id,evb_VendBrandId,evb_VendBrandName');
			//$this->datatables->unset_column('evb_Id');
			$this->datatables->from('ens_vend_brands');
      		echo $this->datatables->generate();
		}else{
			$data[] = array();
			$this->template->set('title', 'EnS Brands');
			$this->template->load('default_layout', 'contents' , 'ens/ensproduct_brands', $data);
		}
	}
	/* Other sup[porting functions below*/
	//jus for update brands of Vend products
	public function updateBrand()
	{
		// Get cURL resource
		$curl = curl_init();
		$header = array();
		$header[] = 'Content-type: application/json';
		$header[] = 'Authorization: Bearer KWDZNSo67gmi5Jofap9rc_UhbFwht8dPOGZq5J8k';
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'https://eggsnsoldiers.vendhq.com/api/2.0/products?page_size=6000',
			CURLOPT_HTTPHEADER => $header
		]);
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		if(!empty($resp)){
			$count = 0;
			$resp = json_decode($resp, true);
			foreach($resp['data'] as $product){
				//echo'<pre>'; print_r($product); echo'</pre>';
				//echo $product['id'];
				if(!empty($product['brand'])){
					$this->app_model->_update_brand($product['id'],$product['brand']);
					//echo $product['brand']['name'].'</br>';
				}else{
					//echo 'No Brand</br>';
				}
				echo $product['id'].'</br>';
				$count++;
			}
			echo '<br>'.$count;
		}
		
		// Close request to clear up some resources
		curl_close($curl);
	}
	
	
	
	
	//jus for update brands of Vend products
	public function checkConnection()
	{
		// Get cURL resource
		$curl = curl_init();
		$header = array();
		$header[] = 'Content-type: application/json';
		$header[] = 'Authorization: Bearer KWDZNSo67gmi5Jofap9rc_UhbFwht8dPOGZq5J8k';
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, [
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => 'https://eggsnsoldiers.vendhq.com/api/2.0/products?page_size=6000',
			CURLOPT_HTTPHEADER => $header
		]);
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		
		if(!empty($resp)){
			$count = 0;
			$resp = json_decode($resp, true);
			//echo '<pre>';print_r($resp['data']);echo '</pre>';die();
			foreach($resp['data'] as $product){
				
				$count++;
			}
			echo '<br>'.$count;
		}
		
		// Close request to clear up some resources
		curl_close($curl);
	}
}
