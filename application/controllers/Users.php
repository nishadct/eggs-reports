<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		if(! $this->session->sessLoggedIn) {
            redirect('/Login');
        }
		if($this->session->sessUserType != 1) {
            redirect('/Dashboard');
        }
		$this->load->model('users_model');
		$this->load->library('datatables');
	}

	public function manage()
	{
		$output = array('status' => 'error','message'=>"",'validation_errors'=>array());
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->form_validation->set_rules('email_input', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('password_input', 'Password', 'trim|required');
			$this->form_validation->set_rules('text_fname', 'Email', 'trim|required');
			$this->form_validation->set_rules('select_company', 'Company', 'trim|required');
			$this->form_validation->set_rules('text_role', 'User Role', 'trim|required');
			//$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if ($this->form_validation->run() == FALSE) {
				$output['validation_errors'] = $this->form_validation->error_array();
				$output['message'] = "Validations fials..";
			}else{
				$userdata = [
					'user_firstname'=>$this->security->xss_clean($this->input->post('text_fname')),
					'user_lastname'=>$this->security->xss_clean($this->input->post('text_lname')),
					'user_email'=>$this->security->xss_clean($this->input->post('email_input')),
					'user_password'=>md5($this->input->post('password_input')),
					'user_type'=>$this->security->xss_clean($this->input->post('text_role')),
					'user_company'=>$this->security->xss_clean($this->input->post('select_company')),
					'user_status'=>'a',
					'user_on'=>date('Y-m-d H:i:s'),
					'user_by'=>$this->session->sessUserId
				];
				$result = $this->users_model->create_users($userdata);
				if($result['status']== true){
					$output['status'] = 'success';
					$output['message'] = 'Logging in. Please wait...';
					$output['url'] = '/Users/manage';
				}else{
					$output['message'] = $result['reason'];
				}
			}
			echo json_encode($output); exit();
		}else{
			$data['companies'] = ['ens','ct'];
			$this->template->set('title', 'Users');
			$this->template->load('default_layout', 'contents' , 'users/manage_users', $data);
		}
	}
	
	public function view()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$this->datatables->select('id,CONCAT(user_firstname, " ", user_lastname) AS username,user_email,user_type,user_company,user_status,user_on');
			//$this->datatables->unset_column('user_lastname');
			//$this->datatables->edit_column('user_firstname','$1','$this->datatable_counter(id,"product")');
			$this->datatables->from('users');
      		echo $this->datatables->generate();
		}else{
			redirect('/Dashboard');
		}
	}
	
}
